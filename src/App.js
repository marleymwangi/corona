import { lazy, Suspense, useEffect, useState } from 'react';
import { prettyPrintStat, sortData } from './utils';
import Navbar from './components/navbar';
import Hero from './components/hero';
import Stats from './components/stats';
import globe from '../src/images/globe.svg';
import SpreadText from './components/spreadText';
import SymptomsText from './components/symptomsText';
import PreventText from './components/preventText';
import Faq from '../src/components/faq';
//import Map from './components/map';
//import CountriesList from './components/countriesList';
const Map = lazy(() => import('./components/map'));
const CountriesList = lazy(() => import('./components/countriesList'));

function App() {
  const [loader, setLoader] = useState(true);
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState({
    name: "Global",
    flag: globe,
    cases: 0,
    recovered: 0,
    deaths: 0,
  });
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [mapZoom, setMapZoom] = useState(2);
  const [mapCountries, setMapCountries] = useState([]);
  const [casesType, setCasesType] = useState("cases");

  useEffect(async () => {
    const getCounrtiesData = async () => {
      return fetch('https://disease.sh/v3/covid-19/countries')
        .then(res => res.json())
        .then(data => {
          if (data) {
            const cntrys = data.map((cntry) => {
              return {
                name: cntry.country,
                code: cntry.countryInfo.iso2,
                flag: cntry.countryInfo.flag,
                cases: prettyPrintStat(cntry.cases),
                caseslng: cntry.cases,
                recovered: prettyPrintStat(cntry.recovered),
                deaths: prettyPrintStat(cntry.deaths),
                coords: {
                  lat: cntry.countryInfo.lat,
                  long: cntry.countryInfo.long
                }
              }
            })

            setMapCountries(data);
            return cntrys;
          }
        });
    };

    const getGlobalData = async () => {
      return fetch("https://disease.sh/v3/covid-19/all")
        .then(res => res.json())
        .then(data => {
          if (data) {
            const global = {
              name: "Global",
              flag: globe,
              cases: prettyPrintStat(data.cases),
              recovered: prettyPrintStat(data.recovered),
              deaths: prettyPrintStat(data.deaths),
            };
            return global;
          };
        });
    };

    //get global values
    let g = await getGlobalData();
    setCountry(g);
    //get contries individual stats
    let c = await getCounrtiesData();
    //sort by cases
    let sorted = c && sortData(c);
    //update countries list
    setCountries(g ? [g].concat(sortData(sorted)) : [])
  }, [])

  useEffect(() => {
    if (country?.name == 'Global') {
      setMapCenter([34.80746, -40.4796]);
      setMapZoom(2);
    } else if (country?.name && country?.name.length > 0) {
      setMapCenter([country.coords.lat, country.coords.long]);
      setMapZoom(4);
    }

  }, [country])

  const removeLoader = () => {
    const loader = document.querySelector('.loader-container');
    loader.classList.add('loader--hide');
  }

  useEffect(() => {
    if (country.cases && country.recovered && country.deaths) {
      if (loader) {
        removeLoader();
        setLoader(false);
      }
    }
  }, [country])

  return (
    <>
      <Navbar />
      <Hero />
      <Stats country={country} setCasesType={setCasesType} />
      <section className="main-content">
        <div className="map-container">
          <Suspense fallback={null}>
            <Map
              countries={mapCountries}
              casesType={casesType}
              center={mapCenter}
              zoom={mapZoom}
            />
          </Suspense>
        </div>
        <div className="list">
          <Suspense fallback={null}>
            <CountriesList countries={countries} country={country} setCountry={setCountry} />
          </Suspense>
        </div>
      </section>
      <section className="main-content-text">
        <SpreadText />
        <SymptomsText />
        <PreventText />
        <Faq />
      </section>
    </>
  );
}

export default App;
