export const spread = [
  {
    imgSrc: 'touch.png',
    title: 'Person-to-person spread',
    text: 'The coronavirus is thought to spread mainly from person to person. This can happen between people who are in close contact with one another.'
  },
  {
    imgSrc: 'breath.png',
    title: 'Person coughs or sneezes',
    text: 'The coronavirus is thought to spread mainly from person to person. This can happen between people who are in close contact with one another.'
  },
  {
    imgSrc: 'cough.png',
    title: 'Sore Throat',
    text: 'A sore throat can make it painful to eat and even talk. It brings scratchiness and irritation to the throat that can become worse when swallowing.'
  }
]

export const symptoms = [
  {
    imgSrc: 'fever.png',
    title: 'Fever',
    text: 'High Fever – this means you feel hot to touch on your chest or back (you do not need to measure your temperature). It is a common sign and also may appear in 2-10 days if you affected.'
  },
  {
    imgSrc: 'cough.png',
    title: 'Dry Cough',
    text: 'Continuous cough – this means coughing a lot for more than an hour, or 3 or more coughing episodes in 24 hours (if you usually have a cough, it may be worse than usual).'
  },
  {
    imgSrc: 'cough.png',
    title: 'Sore Throat',
    text: 'A sore throat can make it painful to eat and even talk. It brings scratchiness and irritation to the throat that can become worse when swallowing.'
  }
]

export const prevention=[
  {
    imgSrc: 'wash.png',
    title: 'Wash your hands regularly with soap',
    text: 'Handwashing is one of the best ways to protect yourself and your family from getting sick. Learn when and how you should wash your hands to stay healthy.'
  },
  {
    imgSrc: 'home.png',
    title: 'Stay home if you feel unwell.',
    text: 'Most people with COVID-19 have mild illness and are able to recover at home without medical care. Do not leave your home, except to get medical care. '
  },
  {
    imgSrc: 'notouch.png',
    title: 'Avoid touching your face.',
    text: 'To help prevent infections, keep your hands away from your eyes, nose, and mouth. Why? Touching the mucous membranes on your face.'
  },
  {
    imgSrc: 'cover.png',
    title: 'Cover your mouth and nose',
    text: "Cover your mouth and nose with a tissue when you cough or sneeze. Put your used tissue in a waste basket. If you don't have a tissue, cough or sneeze into your."
  }
]