import { ReactComponent as Globe } from "../images/globe.svg";
import { ReactComponent as Virus } from "../images/virus.svg";

export default function Stats({country, setCasesType}) {
  return (
    <div className="stats-container">
      <div className="stats">
        <div className="stat">
          <Globe/>
          <div className="details">
            <span>Stats Overview</span>
            <span>Global</span>
          </div>
        </div>
        <div className="stat"
          onClick={()=> setCasesType('cases')}
        >
          <Virus className="red"/>
          <div className="details">
            <span>Total Cases</span>
            <span>{country.cases}</span>
          </div>
        </div>
        <div className="stat"
          onClick={()=> setCasesType('recovered')}
        >
          <Virus className="green"/>
          <div className="details">
            <span>Total Recovered</span>
            <span>{country.recovered}</span>
          </div>
        </div>
        <div className="stat"
          onClick={()=> setCasesType('deaths')}
        >
          <Virus className="gray"/>
          <div className="details">
            <span>Total Deaths</span>
            <span>{country.deaths}</span>
          </div>
        </div>
      </div>
    </div>
  )
}
