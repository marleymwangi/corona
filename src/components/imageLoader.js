import { useState } from 'react'

export default function ImageLoader({ imgSrc }) {
  const [loaded, setLoaded] = useState(false);

  return (
    <div className={`flex flex-grow w-full justify-center items-center 
    overflow-hidden transition-all duration-1000 ease-in-out
    ${loaded? 'opacity-100' : 'opacity-0'}`}>
      <img
        alt=""
        src={imgSrc}
        onLoad={()=>setLoaded(true)}
      />
    </div>
  )
}
