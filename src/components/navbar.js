import { ReactComponent as Virus } from "../images/virus.svg";

export default function Navbar() {
  return (
    <div className="navbar">
      <div className="container">
        <div className="brand">
          <Virus />
          <span>Covid 19</span>
        </div>
        <div className="right">
        </div>
      </div>
    </div>
  )
}
