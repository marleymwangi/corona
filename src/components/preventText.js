import { prevention } from '../data/info';

export default function PreventText() {
  //handles dynamic image loading.. 
  const images = require.context('../images', true);
  const loadImage = imageName => (images(`./${imageName}`).default);

  return (
    <>
      <h5>Prevent coronavirus spread</h5>
      <div className="main-card-container">
        {
          prevention && prevention.map((t, i) => {
            return (
              <div className=" card" key={i}>
                <div className="image">
                  <img src={loadImage(t.imgSrc)} alt="" />
                </div>
                <h6>{t.title}</h6>
                <p>{t.text}</p>
              </div>
            )
          })
        }
      </div>
    </>
  )
}
