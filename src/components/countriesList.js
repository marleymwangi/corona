import numeral from 'numeral';

export default function CountriesList({
  countries, country, setCountry
}) {
  return (
    <div className="countries-list">
      <div className="selected">
        <div className="name">
          <img src={country.flag} alt={country.code} />
          <span>{country.name}</span>
        </div>
        <span>{numeral(country.cases).format("0,0")}</span>
      </div>
      <div className="countries no-scrollbar">
        {countries.map((country, idx) => (
          <div className="cnt" key={idx} 
            onClick={() => setCountry(country)}
          >
            <div className="name">
              <img src={country.flag} alt={country.code} />
              <span>{country.name}</span>
            </div>
            <span>{numeral(country.cases).format("0,0")}</span>
          </div>
        ))}
      </div>
    </div>
  )
}
