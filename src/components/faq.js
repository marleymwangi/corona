import React, { useState } from 'react';
import { faq } from "../data/faq";

export default function Faq() {
  const [topic, setTopic] = useState(0);
  const [quest, setQuest] = useState(0);

  return (
    <div className="main-faq">
      <h5>FAQ</h5>
      <div className="faq-container">
        <div className="topics">
          {
            faq && faq.map((t, i) => (
              <div
                key={i}
                onClick={() => setTopic(i)}
                className={`topic ${topic === i && 'selected'}`}
              >
                <span>{t.topic}</span>
                <svg xmlns="http://www.w3.org/2000/svg" className="icon" viewBox="0 0 20 20" fill="currentColor">
                  <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                </svg>
              </div>
            ))
          }
        </div>
        <div className="questions">
          {
            faq?.length > 0 && faq[topic].questions.map((t, i) => (
              <div
                key={i}
                onClick={() => setQuest(i)}
                className={`q-container ${quest === i && 'selected'}`}
              >
                <div className="question">
                  {t.question}
                </div>
                <div className="divider" />
                <div className="answer">
                  {t.answer}
                </div>
              </div>
            ))
          }
        </div>
      </div>
    </div>
  )
}
