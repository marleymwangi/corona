import { spread } from '../data/info';

export default function SpreadText() {
  //handles dynamic image loading.. 
  const images = require.context('../images', true);
  const loadImage = imageName => (images(`./${imageName}`).default);

  return (
    <>
      <h5>How coronavirus spread</h5>
      <div className="main-card-container">
        {
          spread && spread.map((t, i) => {
            return (
              <div className=" card card-white" key={i}>
                <div className="image">
                  <img src={loadImage(t.imgSrc)} alt="" />
                </div>
                <h6>{t.title}</h6>
                <p>{t.text}</p>
              </div>
            )
          })
        }
      </div>
    </>
  )
}
