import emo from "../images/emo.svg";
import ImageLoader from "./imageLoader";

export default function Hero() {
  return (
    <div className="hero">
      <div className="container">
        <div className="left">
          <>
            <h3>COVID-19 Live Tracker</h3>
            <p>The Coronavirus (COVID-19) was first reported in Wuhan, Hubei, China in December 2019, the outbreak was later recognized as a pandemic by the World Health Organization (WHO) on 11 March 2020.</p>
            <div className="btns">
              <button className="pink">
                How to Protect
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
                </svg>
              </button>
              <button className="trans">
                Find a Doctor
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
                </svg>
              </button>
            </div>
          </>
        </div>
        <div className="right">
          <ImageLoader imgSrc={emo} />
        </div>
      </div>
    </div>
  )
}
